#include <iostream>
#include <stack>

using namespace std;

stack<int> myStack;

int main(){
	ios_base::sync_with_stdio(0);
	myStack.push(0);
	int a;
	cin>>a;
	int x, y;
	int result=0;
	for(int i=0; i<a; ++i){
		cin>>x>>y;
		while(myStack.top()>y)myStack.pop();
		if(myStack.top()!=y)++result;
		myStack.push(y);
	}
	cout<<result;
	return 0;
}
